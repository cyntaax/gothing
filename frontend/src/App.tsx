import React from 'react';
import './App.css';

function App() {
    const socket = new WebSocket("ws://localhost:3031/socket")
    socket.onopen = (sock) => {
        console.log('opened')
        socket.send(JSON.stringify({hello: 'there'}))
    }

    socket.onmessage = (e) => {
        console.log(e.data)
    }
    return (
        <div>
            <h1>Hello there!</h1>
        </div>
    )
}

export default App;
