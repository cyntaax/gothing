package main

import (
	"api/router"
	"api/services/discord"
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"github.com/joho/godotenv"
)

func main() {
	fmt.Println("Hello there")
	if os.Getenv("DISCORD_TOKEN") == "" {
		err := godotenv.Load("../.env")
		if err != nil {
			panic(err)
		}
	}
	server := gin.Default()
	discord.Start()
	err := router.CreateRoutes(server).Run(":3030")
	if err != nil {
		panic(err)
	}
}
