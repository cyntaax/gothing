package controllers

import (
	"api/services/discord"
	"github.com/gin-gonic/gin"
)

func GetCommands(c *gin.Context) {
	cmds := map[string]interface{}{}

	for _, cmd := range discord.App.Commands {
		cmds[cmd.Trigger] = map[string]interface{}{
			"description": cmd.Description,
		}
	}

	discord.App.SendMessageToChannel("ticket-5", "Sent from controller side")

	c.JSON(200, cmds)
}