package router

import (
	"api/controllers"
	"github.com/gin-gonic/gin"
)

func CreateRoutes(server *gin.Engine) *gin.Engine {
	base := server.Group("/")
	{
		base.GET("", controllers.GetTest)
	}

	commands := server.Group("/commands")
	{
		commands.GET("", controllers.GetCommands)
	}
	return server
}