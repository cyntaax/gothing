package discord

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"os"
	"strings"
)

var App DiscordApp

var PendingCommands []*DCommand

type DiscordApp struct {
	Initialized bool
	Commands []*DCommand
	Instance *discordgo.Session
	Guild *discordgo.Guild
}

func (da *DiscordApp) AddCommand(command *DCommand) {
	da.Commands = append(da.Commands, command)
}

func (da *DiscordApp) HandleMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	command := strings.Split(m.Message.Content, " ")[0]
	for _, cmd := range da.Commands {
		tcmd := "!" + cmd.Trigger
		fmt.Println("checking", tcmd, " against ", command)
		if tcmd == command {
			for _, protector := range cmd.Protectors {
				val := protector(MessagePack{
					Session: s,
					Message: m,
				})
				if val == false {
					return
				}
			}
			handleCommand(s,m, strings.Join(strings.Split(m.Message.Content, " ")[1:], " "), cmd.Handler)
		}
	}
}

func(da *DiscordApp) SendMessageToChannel(channelName string, message string) {
	fmt.Println(da.Guild.Channels)
	for _, gchan := range da.Guild.Channels {
		fmt.Println("checking", gchan.Name)
		if gchan.Name == channelName {
			da.Instance.ChannelMessageSend(gchan.ID, message)
		}
	}
}

func Start() {
	if App.Initialized != true {
		client, err := discordgo.New("Bot " + os.Getenv("DISCORD_TOKEN"))
		if err != nil {
			fmt.Println(err)
			return
		}
		client.StateEnabled = true
		fmt.Println("Starting discord")
		App = DiscordApp{
			Initialized: true,
			Instance: client,
		}
		App.Instance.AddHandler(App.HandleMessage)


		mycmd := DCommand{
			Trigger: "test",
			Description: "A test command while I work on a few things",
			Handler: func(msg CommandMessage, tester string) {
				fmt.Println("Got your", tester)
				msg.Session.ChannelMessageSend(msg.Message.ChannelID, "Pong!")
			},
		}

		App.AddCommand(&mycmd)

		for _, cmd := range PendingCommands {
			App.AddCommand(cmd)
		}
		err = App.Instance.Open()
		if err != nil {
			fmt.Println("Failed to open connection")
			return
		} else {
			fmt.Println("Discord instance started")
		}

		var guild *discordgo.Guild

		for _, stateGuild := range App.Instance.State.Guilds {
			fmt.Println("guild", stateGuild.Name)
			if stateGuild.ID == os.Getenv("GUILD") {
				guild = stateGuild
			}
		}

		if guild == nil {
			panic("Failed to get guild")
		}
		App.Guild = guild

	} else {
		fmt.Println("Skip start discord")
	}
}

func GetRoleName(m MessagePack, role string) string {
	guildID := m.Message.GuildID
	roles, _ := m.Session.GuildRoles(guildID)
	for _, guildRole := range roles {
		if guildRole.ID == role {
			return guildRole.Name
		}
	}
	return ""
}

func RequireRoles(m MessagePack, roles ...string) bool {
	total := len(roles)
	matched := 0
	for _, role := range roles {
		for _, memberRole := range m.Message.Member.Roles {
			roleName := GetRoleName(m, memberRole)
			fmt.Println("Checking role", role, memberRole)
			if role == roleName {
				matched++
			}
		}
	}
	if matched != total {
		m.Session.ChannelMessageSend(m.Message.ChannelID, "You do not have the required roles")
	}
	return matched == total
}