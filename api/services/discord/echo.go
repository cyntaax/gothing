package discord

import (
	"fmt"
)

func init() {
	fmt.Println("Init command")
	cmd := DCommand{
		Trigger: "echo",
		Description: "Will echo anything you type after the command",
		Handler: func(m CommandMessage) {
			m.Session.ChannelMessageSend(m.Message.ChannelID, m.Remainder)
		},
	}

	cmd.AddProtector(func(m MessagePack) bool {
		if m.Message.Author.Username != "Cyntaax" {
			m.Session.ChannelMessageSend(m.Message.ChannelID, "You do not have permission to run this")
			return false
		}
		return true
	})

	cmd.AddProtector(func(m MessagePack) bool {
		return RequireRoles(m, "Support")
	})

	PendingCommands = append(PendingCommands, &cmd)
}