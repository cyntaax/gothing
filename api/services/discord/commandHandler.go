package discord

import (
	"github.com/bwmarrin/discordgo"
	"reflect"
	"strconv"
	"strings"
)

type DCommand struct {
	Trigger string
	Handler interface{}
	Description string
	Protectors []func(MessagePack) bool
}


func (dc *DCommand) AddProtector(protector func(MessagePack) bool) {
	if dc.Protectors == nil {
		dc.Protectors = make([]func(MessagePack) bool, 0)
	}

	dc.Protectors = append(dc.Protectors, protector)
}


type CommandMessage struct {
	Content string
	Remainder string
	Session *discordgo.Session
	Message *discordgo.MessageCreate
}

type MessagePack struct {
	Session *discordgo.Session
	Message *discordgo.MessageCreate
}

// TODO: If there is leftover string, put that somewhere

func handleCommand(se *discordgo.Session, m *discordgo.MessageCreate, command string, handler interface{}) {
	commandMessage := CommandMessage{}
	commandMessage.Session = se
	commandMessage.Message = m

	tp := reflect.TypeOf(handler)

	params := tp.NumIn()

	paramKinds := make([]string, 0)

	for i := 0; i < params; i++ {
		parm := tp.In(i)
		paramKinds = append(paramKinds, parm.Kind().String())
	}

	testStr := command

	s := strings.Split(testStr, " ")

	s2 := s

	if len(s2) < (len(paramKinds) - 1)  {
		panic("Not enough arguments")
	}

	finalArgs := make([]reflect.Value, 0)

	for idx, pk := range paramKinds {
		if idx == 0 {
			var remainder string
			if len(s2) > len(paramKinds) {
				remainder = strings.Join(s2[len(paramKinds) - 1:], " ")
			}
			commandMessage.Remainder = remainder
			commandMessage.Content = testStr
			finalArgs = append(finalArgs, reflect.ValueOf(commandMessage))

		} else {
			switch pk {
			case "int64":
				oval, err := strconv.Atoi(s2[idx - 1])
				ooval := int64(oval)
				if err != nil {
					panic(err)
				}
				finalArgs = append(finalArgs, reflect.ValueOf(ooval))
				break
			case "bool":
				if s2[idx - 1] == "true" {
					finalArgs = append(finalArgs, reflect.ValueOf(true))
				} else if s2[idx - 1] == "false" {
					finalArgs = append(finalArgs, reflect.ValueOf(false))
				} else {
					panic("Can't assert to boolean")
				}
				break
			case "string":
				finalArgs = append(finalArgs, reflect.ValueOf(s2[idx - 1]))
				break
			}
		}
	}

	reflect.ValueOf(handler).Call(finalArgs)
}